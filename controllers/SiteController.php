<?php

namespace app\controllers;

use app\models\UrlForm;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new UrlForm();

        $request = Yii::$app->request;
        $shortener = Yii::$app->shortener;

        if ($model->load($request->post()) && $model->validate()) {
            $model->shortUrl = $shortener->createShortUrl($model->url);
        }

        return $this->render('index', ['model' => $model]);
    }

    /**
     * @param string $hash
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionRedirect($hash)
    {
        $shortener = Yii::$app->shortener;
        $url = $shortener->find($hash);
        if (!$url) {
            return $this->render('404');
        }

        return $this->redirect($url);
    }
}
