<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=localhost;dbname=shortener;user=postgres',
    'charset' => 'utf8',
];
