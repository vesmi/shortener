<?php

use yii\db\Migration;

class m161204_140643_initial extends Migration
{
    public function up()
    {
        $this->createTable(
            'url',
            [
                'hash' => 'VARCHAR(32) NOT NULL',
                'url' => 'VARCHAR(1000) NOT NULL',
                'created_at' => 'INTEGER NOT NULL'
            ]
        );

        $this->addPrimaryKey('hash_url_pk', 'url', 'hash');
        $this->createIndex('url_url_idx', 'url', 'url');
    }

    public function down()
    {
        $this->dropTable('url');
    }
}
