<?php

namespace app\components;

use app\models\Url;
use yii\base\Component;
use yii\di\Instance;
use yii\helpers\VarDumper;

class Shortener extends Component
{
    /**
     * @var int Hash length
     */
    public $hashLength = 6;

    /**
     * @var string Base URL for short URLs (example: https://shortener.domain/)
     */
    public $baseUrl;

    /**
     * @var HasherInterface Component for generate hash string
     */
    public $hasher = 'hasher';

    public function init()
    {
        parent::init();
        $this->hasher = Instance::ensure($this->hasher, HasherInterface::class);

        if (!$this->baseUrl) {
            $this->baseUrl = \yii\helpers\Url::home(true);
        }
    }

    /**
     * Creates and returns short URL
     *
     * @param string $longUrl
     * @return string
     * @throws ShortenerException
     */
    public function createShortUrl($longUrl)
    {
        $urlModel = Url::findByUrl($longUrl);

        if (!$urlModel) {
            $urlModel = new Url();
            $urlModel->url = $longUrl;

            $hash = $this->generateHash();
            while (Url::findByHash($hash)) {
                $hash = $this->generateHash();
            }

            $urlModel->hash = $hash;

            if (!$urlModel->save()) {
                throw new ShortenerException(
                    "Couldn't save short url to database: " . VarDumper::dumpAsString($urlModel->getErrors())
                );
            }
        }

        return $this->generateShortenUrl($urlModel->hash);
    }

    /**
     * @param string $hash
     * @return string|null
     */
    public function find($hash)
    {
        $urlModel = Url::findByHash($hash);
        return $urlModel ? $urlModel->url : null;
    }

    /**
     * Returns shorten URL with base domain
     * @param string $hash
     * @return string
     */
    protected function generateShortenUrl($hash)
    {
        return $this->baseUrl . $hash;
    }

    /**
     * Generate hash
     * @return string
     */
    protected function generateHash()
    {
        return $this->hasher->getHash($this->hashLength);
    }
}
