<?php

namespace app\components;

/**
 * Interface HasherInterface
 * @package app\components
 */
interface HasherInterface
{
    /**
     * Generate hash of the specified length
     * @param int $length
     * @return string
     */
    public function getHash($length = 6);
}
