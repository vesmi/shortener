<?php

namespace app\components;

/**
 * Class Hasher
 * @package app\components
 */
class Hasher implements HasherInterface
{

    /**
     * @var string Allowed chars in hash
     */
    public $chars = "1234567890abcdfghijkmnopqrstuvwxyzABCDFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * @param int $length
     * @return string
     */
    public function getHash($length = 6)
    {
        return substr(str_shuffle($this->chars), 0, $length);
    }
}
