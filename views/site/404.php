<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = 'Ссылка не найдена';

?>
<div class="site-error">
    <div class="jumbotron">
        <h1>Упс...</h1>
        <p class="lead">Кажется, по вашей ссылке ничего не найдено.</p>
        <a href="/">Перейти к сокращателю...</a>
    </div>
</div>