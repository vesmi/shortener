<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model \app\models\UrlForm */

$this->title = 'Сокращатель ссылок';
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Сокращатель ссылок</h1>

        <p class="lead">Впиши, сократи.</p>

        <?php Pjax::begin(['id' => 'shortener-pjax']); ?>

        <?php $form = ActiveForm::begin(['options' => ['data-pjax' => '#shortener-pjax']]) ?>

        <?= $form->field($model, 'url')->textInput(['placeholder' => 'Введите URL', 'class' => 'form-control'])->label(false) ?>

        <?= Html::submitButton('Сократить', ['class' => 'btn btn-success form-control', 'id' => 'shortener', 'data-pjax' => '#shortener-pjax']) ?>

        <?php ActiveForm::end() ?>

        <?php if ($model->shortUrl): ?>
            <p class="bg-success">
                <?= Html::a($model->shortUrl, $model->shortUrl, ['target' => '_blank', 'data-pjax' => 0]) ?>
                <?= Html::textInput('short-url-copy', $model->shortUrl, ['id' => 'short-url-copy']) ?>
                <br>
                <?= Html::button(
                    'Скопировать',
                    ['class' => 'btn btn-xs btn-info', 'onclick' => 'copyShortUrl()']
                ) ?>
            </p>
        <?php endif; ?>

        <?php Pjax::end() ?>
    </div>
</div>