<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class Url
 *
 * @property string $hash
 * @property string $url
 * @property integer $created_at
 *
 * @package app\models
 */
class Url extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'url';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => function () {
                    return time();
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'hash'], 'required'],
            ['url', 'string', 'min' => 8, 'max' => 1000],
            ['url', 'url'],

            ['hash', 'string', 'max' => 32],

            ['created_at', 'number'],
        ];
    }

    /**
     * Find by URL
     *
     * @param string $url
     * @return self|null|ActiveRecord
     */
    public static function findByUrl($url)
    {
        return self::find()->where(['url' => $url])->one();
    }

    /**
     * Find by hash
     *
     * @param $hash
     * @return self|null|ActiveRecord
     */
    public static function findByHash($hash)
    {
        return self::find()->where(['hash' => $hash])->one();
    }
}
