<?php

namespace app\models;

use yii\base\Model;

class UrlForm extends Model
{
    public $url;

    public $shortUrl;

    public function rules()
    {
        return [
            ['url', 'required'],
            [['url', 'shortUrl'], 'string', 'min' => 8, 'max' => 1000],
            [['url', 'shortUrl'], 'url'],
        ];
    }
}
