Simple Shortener
============================

Установка
---

1. Склонируйте проект
2. Установите зависимости
```
$ composer install
```
3. Сконфигурируйте приложение:

* Укажите путь до базы в файле `config/db.php`
```
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=localhost;dbname=shortener;user=postgres;password=',
    'charset' => 'utf8',
];
```

* Настройте компоненты сокращателю ссылок в файле `config/web.php` если необходимо:
```
'shortener' => [
    'class' => 'app\components\Shortener',
    'hashLength' => 6, // длина генерируемого хеша,
    'baseUrl' => 'http://your.domain/', // базовый URL для возвращаемых коротких ссылок (если оставить пустым, то будет использоваться текущий домен)
    'hasher' => 'hasher', // компонент генерации хеша
],
'hasher' => [
    'class' => 'app\components\Hasher',
    'chars' => 'aAbBcC...', // перечень символов, разрешенных для генерации хеша 
]
```

4. Запустите миграции
```
$ ./yii migrate
```

Запуск проекта
---

В режиме отладки:
```
$ ./yii serve 0.0.0.0:8080
```

Для nginx:

1. Поправьте файл `shortener.nginx.conf`
2. Прокиньте ссылку на концигурации и перезапустите nginx:
```
$ ln -s /var/www/shortener/shortener.nginx.conf /etc/nginx/sites-enabled/10-shortener
$ nginx reload 
```