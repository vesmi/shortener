<?php

use yii\BaseYii;
use yii\web\Application;

class Yii extends BaseYii
{
    /**
     * @var \yii\console\Application|\yii\web\Application|AutocompleteWebApplication the application instance
     */
    public static $app;
}

/**
 * Class AutocompleteWebApplication
 *
 * @property \app\components\Shortener $shortener
 */
class AutocompleteWebApplication extends Application
{

}
